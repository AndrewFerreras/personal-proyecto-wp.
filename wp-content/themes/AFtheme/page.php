<?php

get_header();

?>
<?php


while (have_posts()) {
  the_post();

?>
<div class="contenedor">
  <h1 class="Titulo"><?= the_title(); ?></h1>
  <main>

  <p class="texto"><?= the_content() ?></p>

  </main>
 

</div>

<?php
}
wp_reset_postdata();
?>

<?php
get_footer();
?>