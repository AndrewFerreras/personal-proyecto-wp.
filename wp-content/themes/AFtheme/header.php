<!DOCTYPE html>

<htm lang="es">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-whidth, user-scalable=no
            initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <title>AF TRAVER</title>
        <link rel="stylesheet" href="css/normalize.css">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap"
            rel="stylesheet">
            <link rel="stylesheet" href="wp-content/themes/AFTHEME/css/estilos.css">
            <script src="https://kit.fontawesome.com/32ac0158d0.js" crossorigin="anonymous"></script>

    </head>


    <body>
        <!-- inicio del header -->
        <header>
            <div class="contenedor">
                <nav class="menu">
                    <ul>
                        <a href="<?= site_url('/')?>">Inicio</a>
                        <a href="<?= site_url('/?page_id=10')?>">Contacto</a>
                        <a href="#servicios">Servicios</a>
                        
                    </ul>

                </nav>
                <div class="contenedor-texto">
                    <div class=texto>
                        <h1 class="titulo">AF TRAVERS</h1>
                        <H2 clas="eslogan">una aventura sin igual</H2>
                    </div>
                </div>
            </div>
        </header>