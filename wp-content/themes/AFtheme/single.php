<?php 
get_header()
?>
<?php


while (have_posts()) {
  the_post();

?>
<main>
            <section class="acerca-de">
                <div class="contenedor">
                    <div class="texto">
                        <h3 class="titulo">
                        <?= the_title(); ?>
                        </h3>
                        <p class="">
                        <?= the_content() ?>
                        </p>
                    </div>
                </div>
            </section>



<?php
}
wp_reset_postdata();
?>

<?php
get_footer();
?>