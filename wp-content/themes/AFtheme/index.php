<?php
get_header();
?>
<!-- fin del header -->

<!-- inicio seccion principal -->
<main>
    <section class="acerca-de">
        <div class="contenedor">
            <div class="texto">
                <h3 class="titulo">
                    Acerca de Nosotros
                </h3>
                <p class="">
                    Somos un grupo de personas comprometidas hacer que tus vacaciones y escapes de la vida
                    cotidiana sean mas placenteras
                </p>
            </div>
        </div>
    </section>

    <section class="trabajos" id="servicios"> 
        <div class="contenedor">
            <h3 class="titulo">Servicios</h3>
            <div class="contenedor-trabajos">
                <div class="trabajo">
                    <div class="thumb">
                        <img src="https://gmkgoa.bl.files.1drv.com/y4mrtCEhNP4pi72jiQr3FkQ0scRPEsVXKH6mWuj-fBcvqPsxRHCEd8W2UXfp7Or-hvvTXN9v9e563NpE9-3YPJHW_CvQb6xl6pnWZSZ_Qg-daB5NyOkYo7sKfcdtjNTGjXjyu_ncxmk7KHZgCnQWL2FJa46wZmxLFlxPcQxqsy6q6tBqYyXwLP-jmPHIw1Y2XbCYKCgnsOSvpW0l4ZFcvFzUg?width=500&height=500&cropmode=none" alt="Excursiones" height="130px">
                    </div>
                    <div class="descripcion">
                        <p class="nombre">Excursiones</p>
                        <p class="categoria">planes de viajes por todo el territorio nacional</p>
                    </div>
                </div>
                <div class="trabajo">
                    <div class="thumb">
                        <img src="https://g2kgoa.bl.files.1drv.com/y4mJrQ21DS09X9avt4rH3H8cbswMiK40mKuXUNgYyX5YSgyTdl_ZvxfZnhgDhzBvov05BpqilRdv1XC7XzONbVctPwPZBlf0XQAo138W61Q2dUzAp9vOKbVbz7rwuJoyGDU4H5vgw-Jbg3jgATpVxAkk_398qPSOQ-1d6SkZRLx1KJNig-RL3CRzzRGIIV26u2SFqLjwqwPy-9jwiswYYdgNA?width=500&height=500&cropmode=none" alt="Cruceros">
                    </div>
                    <div class="descripcion">
                        <p class="nombre">Cruceros</p>
                        <p class="categoria">Viajes totalmente seguros a cualquier parte de caribe</p>
                    </div>
                </div>
                <div class="trabajo">
                    <div class="thumb">
                        <img src="https://ggnqtg.bl.files.1drv.com/y4mWITQbIboQGIhVdv7ej2rX_Uo9j9hGBU9xkV_I1laJ6BUOwupjkrR1dRHgGS_oPODrhMH2Z6-QO7Gf5S63folAy0K5I-KLzr5bYb-d9k4finI4iHuiF9V5J3WHjcVQ0pu50n3k9ZZytSwgKmG5FfsTI328M_H3UL0EtiVwOdPwLlT-YZaofL54QAVvjE6VSpY4RJvOti7d81w9_9ThpNFSA?width=500&height=500&cropmode=none" alt="Fiestas">
                    </div>
                    <div class="descripcion">
                        <p class="nombre">Fiestas</p>
                        <p class="categoria">Organizamos los mejores eventos, nuestro trabajo es hacer
                            realidad tu sueño</p>
                    </div>
                </div>
                <div class="trabajo">
                    <div class="thumb">
                        <img src="https://i2lrkw.bl.files.1drv.com/y4mAdBfYoS30wrncJtffZByl_w2k9J4x3yi8JXDMfTkqAjjPWp4ljuyyT6zcc4a4wPrvDnT244Y2K2ePWxC_W_EqAnh7STpQ9vNB6k1EAKfLhv0c_QggaIL5nXjOWg6H90PujguZOOvkkqW_cVLn6bk2SlxEn0UjvfUENkjbWfdNJcotp3isiwBchpZCKOjwIZ6yPP_GAzSMPkL9sO9hjhZ4A?width=500&height=500&cropmode=none" alt="Hoteles">
                    </div>
                    <div class="descripcion">
                        <p class="nombre">Resorts</p>
                        <p class="categoria">Planes de estadia en todo los hoteles del territorio nacional
                            con precios competitivos</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="Articulos">
   
        <div class="contenedor">
            <h3 class="titulo">Articulos</h3>
            <div class="contenedor-trabajos">
            <?php
            while(have_posts())
            {
              the_post();
              
            
        ?>
                <div class="trabajo">
                    
                    <div class="thumb">
                    <img src="https://vznenq.bl.files.1drv.com/y4mY6cR-DtzdE_V_R3vR0FJuaQ6lEZKwaVLQHV7AHu7IUv4sKr7inBjUzvVo2GpLdDkveY_lx6MGP2NQTsNgh7_nLxzW73CpgCui4QyNyomEwOsWKclk7Sy7TD5eB30t6DHyfI_JEAlMj65LjA0X5IQscN4CNkhdcXTRz9QX-wx8sr0kaxVsAvIR10DeHPN1ghwKQwYdFP-WiNwV8WFzm0C7w?width=256&height=192&cropmode=none" alt="Image" >
                    </div>
                    <div class="descripcion">
                        <p class="nombre"><?= the_title(); ?></p>
                        <p class="categoria"> <?= wp_trim_words(get_the_content(), 20); ?></p>
                        <a href="<?php the_permalink(); ?>" class="categoria">Leer mas...</a>
                    </div>
                    
                </div>
                <?php 
        }
          wp_reset_postdata();
        ?>
            </div>
        </div>
    </section>
    </div>
   
    <?php

    get_footer();
    ?>